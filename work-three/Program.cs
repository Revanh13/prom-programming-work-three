using Microsoft.EntityFrameworkCore;
using System.Text.Json;
using work_three;
using work_three.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddDbContext<DataContext>();


var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

app.MapGet("/get_token", async (DataContext data) =>
{    
    var token = Guid.NewGuid().ToString();

    var user = new User()
    {
        Guid = token
    };

    await data.Users.AddAsync(user);
    await data.SaveChangesAsync();
    
    return new { token };
});

app.MapGet("/goods", async (string token, DataContext data) =>
{
    if (token == null)
        return Results.BadRequest("Token must be present");

    var user = await data.Users.SingleOrDefaultAsync(x => x.Guid == token);

    if (user is null)
        return Results.BadRequest("Token is invalid");

    var goods = await data.Products.Where(x => x.UserId == user.Id).ToListAsync();

    return Results.Ok(goods);
});

app.MapPost("/new_good", async (string token, Product product, DataContext data) =>
{
    if (token == null)
        return Results.BadRequest("Token must be present");

    var user = await data.Users.SingleOrDefaultAsync(x => x.Guid == token);
    if (user is null)
        return Results.BadRequest("Token is invalid");

    if (product.Amount <= 0)
        return Results.BadRequest("Amount must be more than 0");

    if (product.Price <= 0)
        return Results.BadRequest("Price must be more than 0");

    product.UserId = user.Id;

    await data.Products.AddAsync(product);
    await data.SaveChangesAsync();

    return Results.Ok();
});

app.Run();