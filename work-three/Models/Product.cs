﻿namespace work_three.Models
{
    public class Product
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public string Name { get; set; } = string.Empty;

        public float Price { get; set; }

        public int Amount { get; set; }
    }
}
