﻿using Microsoft.EntityFrameworkCore;
using work_three.Models;

namespace work_three
{
    public class DataContext : DbContext
    {
        public DbSet<User> Users => Set<User>();

        public DbSet<Product> Products => Set<Product>();

        public DataContext() => Database.EnsureCreated();

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=data.db");
        }
    }
}
